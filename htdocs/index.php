<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Michel Tiok-Hong, Somatopathe</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link href='http://fonts.googleapis.com/css?family=Antic+Slab|Della+Respira|Cinzel' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
                <p class="chromeframe">Votre navigateur est <strong>obsol&egrave;te</strong>. Merci <a href="http://browsehappy.com/">de le mettre &agrave; jour.</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-inverse navbar-static-top" id="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <?php include ("header.html"); ?>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="span9">
                    <?php
                    include ("accueil.html");
                    include ("somato.html");
                    include ("bienfaits.html");
                    include ("parcours.html");
                    include ("liens.html");
                    ?>
                </div>
                <div class="span3 sidebar">
                    <?php
                    include ("sidebar.html");
                    ?>
                </div>
            </div>

            <div class="row footer">
                <?php
                include ("footer.html");
                ?>
            </div>
            <footer>
                <p class="text-center">Tout droit r&eacute;serv&eacute; &copy; 2013</p>
            </footer>
        </div> <!-- #container -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <script>
// Google analytics
//            var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
//            (function(d, t) {
//                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
//                g.src = '//www.google-analytics.com/ga.js';
//                s.parentNode.insertBefore(g, s)
//            }(document, 'script'));
//        </script>
    </body>
</html>
